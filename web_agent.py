# -*- coding: utf-8 -*-

import os
import sys

lib_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'venv/lib64/python2.7/site-packages'))
sys.path.insert(0,lib_path)

import datetime
from pandas import DataFrame
import time


class web_agent():
    def __init__(self):
        start_time = time.time()
        self.nas_dir = sys.argv[1]
        file='./last_date.log'
        if os.path.isfile(file):
           print("file exist" )
        else:
            self.end_date_out_file("2020-01-01 00:00:00")

            self.variale_setting()
            # self.date_parsing()
            self.parsing()
        print(" -------- %s seconds ---------" % (time.time() - start_time))


    def end_date_out_file(self, date):
        output_filename = os.path.normpath("last_date.log")
        with open(output_filename, "w") as out_file:
            out_file.write(str(date))

    def start_date_formatting(self, date):
        print("start date !!", date )
        # print(date[-4:])
        # key값 포맷으로 변환
        date=date[:-4]+"0:00"
        return date

    def date_parsing(self,date_time_str):

        date_time_str=date_time_str[1:]
        date_time_obj = datetime.datetime.strptime(date_time_str, '%d/%b/%Y:%H:%M:%S')

        return date_time_obj

    def init_count(self):
        self.localpay_order_payment_qrcode_count = 0
        self.localpay_order_payment_cancel_count = 0
        self.localpay_order_wallet_variation_charge_count = 0



    def variale_setting(self):

        self.lp_df_init()

        self.first_in=True
        self.last_index=self.get_last_index()

        #10분 간격으로 통계
        self.next_date_min=10
        self.line_count=0


        self.limit_count=0

        self.init_count()
        self.current_time = datetime.datetime.now()
        self.current_date = datetime.datetime.today().strftime("%Y-%m-%d")

        last_date = self.get_last_date()[0:10]
        last_date = datetime.datetime.strptime(last_date, "%Y-%m-%d")

        if str(last_date) < self.current_date :
            self.end_index_out_file(0)
            self.last_index=0

    def lp_df_init(self):
        lp_df_temp = {'id': []}
        self.lp_df = DataFrame(lp_df_temp,
                          columns=['date', 'api_kind'],
                          index=lp_df_temp['id'])

        self.lp_api_call_df = DataFrame(lp_df_temp,
                          columns=['date', 'localpay_order_payment_qrcode_count', 'localpay_order_payment_cancel_count', 'localpay_order_wallet_variation_charge_count'],
                          index=lp_df_temp['id'])

        # self.lp_df = self.lp_df.fillna(0).astype(int)
    def next_date(self,date,min):
        next_date= date + datetime.timedelta(minutes=min)
        # print("self.limit_date:", end_count_date)
        return next_date


    # 이 부분을 파일로 하면 된다.
    def db_to_count(self):
        self.lp_df_init()
        self.lp_api_call_df.loc[0, 'date'] = self.start_count_date
        self.lp_api_call_df.loc[0, 'localpay_order_payment_qrcode_count']= self.localpay_order_payment_qrcode_count
        self.lp_api_call_df.loc[0, 'localpay_order_payment_cancel_count'] = self.localpay_order_payment_cancel_count
        self.lp_api_call_df.loc[0, 'localpay_order_wallet_variation_charge_count'] = self.localpay_order_wallet_variation_charge_count



        # # DataFrame 이나 Serises 를 txt 파일로 깔끔하게 바꿀경우 (이건 tsv)
        # 모든 agent는 시간 관계 없이 그냥 nas에 같은 파일에 append

        # 여기 디렉토리 위치를 nas위치 변경
        self.lp_api_call_df.to_csv(self.nas_dir ,mode='a', index=False, header=None)
        # mode='a' 는 append
        #
        #
        # 이름이 to_csv 이지 그냥 text 형태로 저장되는 것이면 이걸 사용
        #
        #
        #
        # index = False : 자동으로 가장 왼쪽 컬럼에 생성된 0 부터 시작하는 인덱스 지울 때
        #
        # header = None : 헤더 이름 지울 때
        #
        # sep = "\t" : CSV 파일 기본이 comma 라서, 별도의 구분자를 두려면 변경. 예제는 탭(\t) 으로 바꿔 줌

        self.start_count_date=self.next_date(self.start_count_date,self.next_date_min)

#
    def get_last_index(self):
        try:
            with open("last_index.log", "r") as index:
                last_index = index.readline()

                print("last_index:!!", last_index)
        except:
            last_index=0
        return int(last_index)

    def get_last_date(self):
        with open("last_date.log", "r") as index:
            last_date = index.readline()
        return last_date

    def end_index_out_file(self,p):
        output_filename = os.path.normpath("last_index.log")
        # Overwrites the file, ensure we're starting out with a blank file
        with open(output_filename, "w") as out_file:
            out_file.write(str(p))

    def parsing(self):
        # print("self.end_count_date: ", self.end_count_date)
        #test
 	#with open("/var/log/nginx/test/access.log", "r") as in_file:
        with open("/var/log/nginx/access.log", "r") as in_file:
            while 1:
                lines = in_file.readlines(100000)  # 메모리가 허용하는 적당한 양
                # print("len line ", len(lines))

                if not lines:
                    break
                time.sleep(1)

                self.parsing_rotate(lines)


    def parsing_rotate(self, lines):

        for i in range(len(lines)):
           self.line_count += 1
           # print("self.last_index: ", self.last_index)
           # last index보다 앞에 있으면 패스
           if self.line_count <= self.last_index:

               continue

           if self.line_count > 2 and len(lines[i]) > 3:

               item = lines[i].split(" ")


               if self.first_in:
                   self.start_count_date = self.date_parsing(self.start_date_formatting(item[3]))

                   # print(self.start_count_date, self.current_time)
                   self.end_count_date = self.next_date(self.start_count_date, self.next_date_min)
                   self.first_in = False

               self.lp_df.loc[i, 'date'], self.lp_df.loc[i, 'api_kind'] = self.date_parsing(item[3]), item[6]

               # print("self.lp_df.loc[i,'date'], self.end_count_date:", self.lp_df.loc[i,'date'], self.end_count_date)
               if self.lp_df.loc[i, 'date'] < self.end_count_date:
                       # QR 결제 카운트
                   if (self.lp_df.loc[i, 'api_kind'] == "/localpay/order/payment/qrcode"):
                       self.localpay_order_payment_qrcode_count += 1

                   # QR 결제 취소
                   elif (self.lp_df.loc[i, 'api_kind'] == "/localpay/order/payment/cancel"):
                       self.localpay_order_payment_cancel_count += 1

                   # 충전
                   elif (self.lp_df.loc[i, 'api_kind'] == "/localpay/order/wallet/variation/charge"):
                       self.localpay_order_wallet_variation_charge_count += 1



               # agent 실행 했던 시간까지만 수집
               elif self.lp_df.loc[i, 'date'] > self.current_time:
                   print("agent parsing success. ")
                   break

               else:
                   # print("self.start_count_date!!", self.start_count_date)
                   # print("self.end_count_date!!", self.end_count_date)
                   self.db_to_count()
                   self.end_index_out_file(self.line_count)
                   # self.lp_df.to_sql('lp', self.engine_lp, if_exists='append')

                   # if
                   print("append: ", self.start_count_date)
                   self.end_count_date = self.next_date(self.end_count_date, self.next_date_min)



                   self.init_count()
                   i -= 1


    # 사용안함
    def get_last_log_date(self):
            sql = "select date from lp order by date desc limit 1"
            rows=self.engine_lp.execute(sql).fetchall()
            if len(rows) != 0:
                last_crawling_date = datetime.datetime.strptime(rows[0][0], "%Y-%m-%d %H:%M:%S")
                print("get_last_crawling_date last_crawling_date :",rows[0][0])
                # 2020-04-03 16:10:09
                # date_time_obj = datetime.datetime.strptime(date_time_str, '%d/%b/%Y:%H:%M:%S')

                return last_crawling_date
            else:
                return False



if __name__ == "__main__":

    web_agent()
