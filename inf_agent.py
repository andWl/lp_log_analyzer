# -*- coding: utf-8 -*-



import os
import sys

lib_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'venv/lib64/python2.7/site-packages'))
sys.path.insert(0,lib_path)
import time
import datetime
from pandas import DataFrame

class web_agent():
    def __init__(self):
        self.target_dir = [sys.argv[1], sys.argv[2]]
        self.nas_dir = sys.argv[3]
        self.zero_end_index_out_file()
        self.variale_setting()
        # self.date_parsing()
        self.parsing()
        # 하루주기로 갱신이 아니기 때문에 파일별 인덱스 관리


    # def start_date_formatting(self, date): g
    #     print("start date !!", date)
    #     # print(date[-4:])
    #     # key값 포맷으로 변환
    #     # date = date[:-4] + "0:00"
    #     # print("date :! ", date)
    #     return date

    def date_parsing(self, item):

        date_temp = item[0] + "/" + item[1]
        # print("date_temp : ", date_temp)
        # date_time_str = date_time_str[1:]
        # print("date_temp :", date_temp)

        date_time_obj = datetime.datetime.strptime(date_temp, '%Y-%m-%d/%H:%M:%S.%f')
        date_time_obj = datetime.datetime.strftime(date_time_obj, '%Y-%m-%d %H:%M:%S')

        date_time_obj = date_time_obj[:-4] + "0:00"
        date_time_obj = datetime.datetime.strptime(date_time_obj, '%Y-%m-%d %H:%M:%S')
        # print("date_time_obj" , date_time_obj)
        return date_time_obj

    def init_count(self):
        self.localpay_order_payment_card_count = 0
        self.localpay_order_payment_card_cancel_count = 0

    def file_setting(self):
        # last_index , nas_dir 추가
        file=['./log/done.log', self.nas_dir,'./last_index.log']
        for target_log in file:
            if not os.path.isfile(target_log):
                print("file_setting! : ", target_log)
                f = open(target_log, 'w')
                f.close

    def variale_setting(self):

        self.file_setting()
        self.lp_df_init()



        self.first_in = True
        self.last_index = self.get_last_index()

        # 10분 간격으로 통계
        self.next_date_min = 10
        # self.line_count = 0

        self.limit_count = 0

        self.init_count()
        self.current_time = datetime.datetime.now()
        # print("socket.gethostname(): ,", socket.gethostname())
        # list에 맞게 변경해야한다.
        # self.analyze_target_dir=[sys.argv[1],sys.argv[2]]
        #     1번서버 호스트네임으로 구분
        #      /addition/localpay-payment-01
        #      /addition/localpay-payment-03

        #     2번서버
        #     /addition/localpay-payment-00
        #     /addition/localpay-payment-02

        # 테스트
       # self.log_dir1 = ['/Users/jake/project/lp_log_analyzer/agent/log/test1/', '/Users/jake/project/lp_log_analyzer/agent/log/test2/']
       # self.log_dir2=['/addition/localpay-payment-00', '/addition/localpay-payment-02']
        # 아래 주석 풀어 상용

        # self.log_dir2 = ['/addition/localpay-payment-00/', '/addition/localpay-payment-02/']

    def lp_df_init(self):
        lp_df_temp = {'id': []}
        self.lp_df = DataFrame(lp_df_temp,
                               columns=['date', 'api_kind'],
                               index=lp_df_temp['id'])

        self.lp_api_call_df = DataFrame(lp_df_temp,
                                        columns=['date', 'localpay_order_payment_card_count',
                                                 'localpay_order_payment_card_cancel_count'],
                                        index=lp_df_temp['id'])

        # self.lp_df = self.lp_df.fillna(0).astype(int)

    def next_date(self, date, min):
        next_date = date + datetime.timedelta(minutes=min)
        # print("self.limit_date:", end_count_date)
        return next_date

    # 이 부분을 파일로 하면 된다.
    def db_to_count(self):
        self.lp_df_init()
        self.lp_api_call_df.loc[0, 'date'] = self.start_count_date
        self.lp_api_call_df.loc[0, 'localpay_order_payment_card_count'] = self.localpay_order_payment_card_count
        self.lp_api_call_df.loc[0, 'localpay_order_payment_card_cancel_count'] = self.localpay_order_payment_card_cancel_count


        # # DataFrame 이나 Serises 를 txt 파일로 깔끔하게 바꿀경우 (이건 tsv)
        # 모든 agent는 시간 관계 없이 그냥 nas에 같은 파일에 append

        # 여기 디렉토리 위치를 nas위치 변경
        self.lp_api_call_df.to_csv(self.nas_dir, mode='a', index=False, header=None)
        # mode='a' 는 append
        #
        #
        # 이름이 to_csv 이지 그냥 text 형태로 저장되는 것이면 이걸 사용
        #
        #
        #
        # index = False : 자동으로 가장 왼쪽 컬럼에 생성된 0 부터 시작하는 인덱스 지울 때
        #
        # header = None : 헤더 이름 지울 때
        #
        # sep = "\t" : CSV 파일 기본이 comma 라서, 별도의 구분자를 두려면 변경. 예제는 탭(\t) 으로 바꿔 줌

        self.start_count_date = self.next_date(self.start_count_date, self.next_date_min)

    #
    def get_last_index(self):
        try:
            with open("last_index.log", "r") as index:
                last_index = index.readline()
                print("last_index:!!", last_index)
        except:
            last_index = 0
        return int(last_index)

    # def end_index_out_file(self):
    #     output_filename = os.path.normpath("last_index.log")
    #     # Overwrites the file, ensure we're starting out with a blank file
    #     with open(output_filename, "w") as out_file:
    #         out_file.write(str(self.line_count))

    def zero_end_index_out_file(self):
        output_filename = os.path.normpath("last_index.log")
        # Overwrites the file, ensure we're starting out with a blank file
        with open(output_filename, "w") as out_file:
            out_file.write(str(0))

    def find_log_file(self):
        # if socket.gethostname() == self.host_list[0]:
        # self.target_dir = self.log_dir
        print("target_dir :", self.target_dir)
        # else:
        #     self.target_dir = self.log_dir2
        #     # 두개의 디렉토리
        for i in range(2):
            self.current_target_dir=self.target_dir[i]
            # 디렉토리 리스트 출력
            file_list=os.listdir(self.current_target_dir)
            print("file_list:", file_list)
            target_log_list_temp = []
            for s in file_list:
                # "payment.log" string을 포함한 파일만 추출
                # zip파일은 제거해야할듯
                if 'payment.log' in s:
                    # payment.log는 현재 쌓이고 있는 로그이기 때문에 append하지 않는다.
                    if s != "payment.log":
                        #payment를 제외한 모든 payment가 들어간 모든 파일명을 리스트에 담는다.
                        target_log_list_temp.append(s)


            print("target file: ", target_log_list_temp)

            target_log_list = []
            with open("log/done.log", "r") as in_file:
                lines = in_file.readlines()

                # 개행 제거
                for i in range(len(lines)):
                    lines[i]=lines[i].rstrip('\n')

                #
                print("target_log_list_temp: ", target_log_list_temp)
                for j in target_log_list_temp:
                    if j not in lines:
                        print("append log file!!! :",lines)
                        # print("j", j)
                        target_log_list.append(j)
                # print(in_fi)
                # print("in_file!!!", lines)
            print("target_log_list", target_log_list)
            return target_log_list

    def done_log_out_file(self, file_name):
        output_filename = os.path.normpath("log/done.log")
        # Overwrites the file, ensure we're starting out with a blank file
        with open(output_filename, "a") as out_file:
            out_file.write(file_name+'\n')

    def parsing(self):
        # print("self.end_count_date: ", self.end_count_date)
        target_log_list=self.find_log_file()
        print("target_log_list : !!", target_log_list)
        for target in target_log_list:
            # host별로 구분해야한다.
            #     2번서버
            #     /addition/localpay-payment-00
            #     /addition/localpay-payment-02
            #     1번서버 호스트네임으로 구분
            #      /addition/localpay-payment-01
            #      /addition/localpay-payment-03
            print("self.current_target_dir+target: ", self.current_target_dir+target)
            with open(self.current_target_dir+target, "r") as in_file:
                while 1:
                    lines = in_file.readlines(100000)  # 메모리가 허용하는 적당한 양
                    # print("len line ", len(lines))

                    if not lines:
                        break

                    self.parsing_rotate(lines)
                    time.sleep(1)
            self.done_log_out_file(target)


    def parsing_rotate(self, lines):

        for i in range(len(lines)):
            # self.line_count += 1
            # print(self.line_count)
            # print("self.last_index: ", self.last_index)
            # last index보다 앞에 있으면 패스

            # line count도 제외한다. 한번에 다읽는다.
            # if self.line_count <= self.last_index:
            #     continue

            if len(lines[i]) > 3:

                #  inf 로그는 띄어쓰기가 안되어 있어서 split 불가
                item = lines[i].split(" ")
                # print(item)
                system_date_format = '%Y-%m-%d'
                try:
                    datetime.datetime.strptime(item[0], system_date_format)
                    # print(item[0] + " : O.K.(" + input_date.strftime(system_date_format) + ")")
                except ValueError:
                    # print(item[0] + " : NOT O.K.")
                    continue

                if self.first_in:

                    self.start_count_date = self.date_parsing(item)

                    # print(self.start_count_date, self.current_time)
                    self.end_count_date = self.next_date(self.start_count_date, self.next_date_min)
                    self.first_in = False
                # print(1)
                self.lp_df.loc[i, 'date'] = self.date_parsing(item)
                if self.lp_df.loc[i, 'date'] < self.end_count_date:
                    # 카드 결제
                    if "grmCd=02" in lines[i]:
                        # print("[0200]")
                        self.localpay_order_payment_card_count+=1
                    # 카드 취소
                    elif "grmCd=04" in lines[i]:
                        # print("Okay")
                        self.localpay_order_payment_card_cancel_count+=1

                    # agent 실행 했던 시간까지만 수집

                #실시간이 아니기 때문에 break 없이 그냥 한파일 통으로 읽는다. 로그 주기가 파일 당 10분 채 안된다.
                # elif self.lp_df.loc[i, 'date'] > self.current_time:
                #     print("agent parsing success. ")
                #     break

                else:
                    # print("self.start_count_date!!", self.start_count_date)
                    # print("self.end_count_date!!", self.end_count_date)
                    self.db_to_count()
                    # self.end_index_out_file()
                    # self.lp_df.to_sql('lp', self.engine_lp, if_exists='append')

                    # if
                    print("append: ", self.start_count_date)
                    self.end_count_date = self.next_date(self.end_count_date, self.next_date_min)

                    self.init_count()
                    i -= 1

    # 사용안함
    def get_last_log_date(self):
        sql = "select date from lp order by date desc limit 1"
        rows = self.engine_lp.execute(sql).fetchall()
        if len(rows) != 0:
            last_crawling_date = datetime.datetime.strptime(rows[0][0], "%Y-%m-%d %H:%M:%S")
            print("get_last_crawling_date last_crawling_date :", rows[0][0])
            # 2020-04-03 16:10:09
            # date_time_obj = datetime.datetime.strptime(date_time_str, '%d/%b/%Y:%H:%M:%S')

            return last_crawling_date
        else:
            return False


if __name__ == "__main__":
    web_agent()
