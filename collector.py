# -*- coding: utf-8 -*-

import sys
import os

# lib_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'venv/lib64/python2.7/site-packages'))
# sys.path.insert(0,lib_path)


import datetime
import sqlite3
import pandas as pd

from pandas import DataFrame


class collector():
    def __init__(self):
        # self.web_agent_read_data_list = ['./log/agent1.csv',
        #                                  './log/agent2.csv']
        # self.inf_agent_read_data_list = ['./log/inf_agent1.csv',
        #                                  './inf_agent2.csv']

        self.web_agent_read_data_list=['/upload/data_backup/LP-PRD-WEB-API-01/loganal/agent1.csv','/upload/data_backup/LP-PRD-WEB-API-01/loganal/agent2.csv']
        self.inf_agent_read_data_list=['/upload/data_backup/LP-PRD-WEB-API-01/loganal/inf_agent1.csv','/upload/data_backup/LP-PRD-WEB-API-01/loganal/inf_agent2.csv']
        self.file_setting()
        self.variable_setting()
        self.db_setting()
        self.agent_data_input()
        self.inf_agent_data_input()

    #     마지막은 엑셀파일들 비워야해, 안그러면 중복체크를 계속하게된다. "
        self.rename_file()

    def file_setting(self):
        # last_index , nas_dir 추가
        file=[ self.web_agent_read_data_list[0], self.web_agent_read_data_list[1],self.inf_agent_read_data_list[0],self.inf_agent_read_data_list[1]]
        for target_log in file:
            if not os.path.isfile(target_log):
                print("file_setting! : ", target_log)
                f = open(target_log, 'w')
                f.close

    def rename_file(self):
        # try:
        try:
            source=self.web_agent_read_data[0]
            destination=self.web_agent_read_data[0]+"_"+str(self.today)
            os.rename(source,destination)

            source = self.web_agent_read_data[1]
            destination =self.web_agent_read_data[1] + "_" + str(self.today)
            os.rename(source, destination)

            source = self.inf_agent_read_data_list[0]
            destination = self.inf_agent_read_data_list[0] + "_" + str(self.today)
            os.rename(source, destination)

            source = self.inf_agent_read_data_list[1]
            destination = self.inf_agent_read_data_list[1] + "_" + str(self.today)
            os.rename(source, destination)

        except:
            print("file not found")
        # os.system("rm -rf output/agent.csv output/"+ "agent"+str(self.today))
        # except:
        #     print("file not found")





    def variable_setting(self):


        # 2020-04-03 03:40:00
        today_temp= datetime.datetime.now().strftime("%Y-%m-%d")

        # test용 -> 위에 주석을 풀고 아래 주석 해야함
#       today_temp='2020-04-05'

        today_temp=today_temp+" 00:00:00"
        self.today=datetime.datetime.strptime(today_temp, "%Y-%m-%d %H:%M:%S")
        print(self.today)

        lp_df_temp = {'id': []}
        self.lp_api_call_df = DataFrame(lp_df_temp,
                                        columns=['date', 'localpay_order_payment_qrcode_count',
                                                 'localpay_order_payment_cancel_count',
                                                 'localpay_order_wallet_variation_charge_count',
                                                 'localpay_order_payment_card_count',
                                                 'localpay_order_payment_card_cancel_count'
                                                 ],
                                        index=lp_df_temp['id'])

        # print("self.agent_file: ", self.agent_file)
        # print(type(self.agent_file_list))
        # print(self.lp_api_call_df)
        # self.lp_api_call_df=self.agent_file
        # print(self.agent_file.iloc[0][0])


    def db_setting(self):
        #if sys.platform == 'linux':
            # path 추가
        self.db_name = "/root/loganal/agent/collect.db"
        #elif sys.platform == 'darwin':
        #    self.db_name = "/Users/jake/project/lp_log_analyzer/agent/collect.db"

        con = sqlite3.connect(self.db_name)
        cur = con.cursor()

        sql = "select date from lp_collector where date=? group by date"
        cur.execute(sql, (self.today,))
        rows = cur.fetchall()

        if len(rows)==0:
            next_date=self.today
            i=0
            while 1:
                self.lp_api_call_df.loc[i, 'date'] = next_date
                self.lp_api_call_df.loc[
                    i, 'localpay_order_payment_qrcode_count'] = 0
                self.lp_api_call_df.loc[
                    i, 'localpay_order_payment_cancel_count'] = 0
                self.lp_api_call_df.loc[
                    i, 'localpay_order_wallet_variation_charge_count'] = 0
                self.lp_api_call_df.loc[
                    i, 'localpay_order_payment_card_count'] = 0
                self.lp_api_call_df.loc[
                    i, 'localpay_order_payment_card_cancel_count'] = 0

                # print(self.today + datetime.timedelta(days=1))
                next_date += datetime.timedelta(minutes=10)
                # print("next_date: ", next_date)
                if next_date >= self.today + datetime.timedelta(days=1):
                    print(self.today + datetime.timedelta(days=1))
                    break
                i+=1
            print(self.lp_api_call_df)
            self.lp_api_call_df.to_sql('lp_collector', con, if_exists='append')

        # 데이타 Fetch
        # rows 는 list안에 튜플이 있는 [()] 형태로 받아온다
        print("rows :", rows)
        con.close()

    def agent_data_input(self):

        con = sqlite3.connect(self.db_name)
        cur = con.cursor()
        # print("agent_file !!" , self.agent_file.iloc[0][3])
        # print(self.agent_file)
        for target in self.web_agent_read_data_list:
            inf_agent_file_list = pd.read_csv(target)
            for agent_file in inf_agent_file_list:
                for i in range(len(agent_file)):

                    sql = "UPDATE lp_collector SET " \
                          "localpay_order_payment_qrcode_count=localpay_order_payment_qrcode_count+? ," \
                          "localpay_order_payment_cancel_count=localpay_order_payment_cancel_count+? ," \
                          "localpay_order_wallet_variation_charge_count=localpay_order_wallet_variation_charge_count+?" \
                          "WHERE date=?"
                    cur.execute(sql, (agent_file.iloc[i][1],agent_file.iloc[i][2],agent_file.iloc[i][3],agent_file.iloc[i][0]))
                    con.commit()

    def inf_agent_data_input(self):
        con = sqlite3.connect(self.db_name)
        cur = con.cursor()
        for target in self.web_agent_read_data_list:
            self.inf_agent_file_list = pd.read_csv(target)
            for inf_agent_file in self.inf_agent_file_list:
                for i in range(len(inf_agent_file)):
                    print(i)
                    # time = datetime.datetime.strptime(self.inf_agent_file.iloc[i][0], "%Y.%m.%d %H:%M")
                    time = inf_agent_file.iloc[i][0]
                    print("time : ", time)
        #'2020-04-05 12:50:00:00'
                   # time = datetime.datetime.strptime(time, "%Y.%m.%d %H:%M:%S")

                    time = datetime.datetime.strptime(time, "%Y-%m-%d %H:%M:%S")
                    print("inf_agent_file !!", time)
                    print("self.inf_agent_file.iloc[i][1] ", inf_agent_file.iloc[i][1])
                    sql = "UPDATE lp_collector SET " \
                          "localpay_order_payment_card_count=localpay_order_payment_card_count+? ," \
                          "localpay_order_payment_cancel_count=localpay_order_payment_card_cancel_count+?" \
                          "WHERE date=?"
                    cur.execute(sql, (inf_agent_file.iloc[i][1],inf_agent_file.iloc[i][2],time))
                    con.commit()

if __name__ == "__main__":

    collector()
