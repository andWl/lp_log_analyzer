# -*- coding: utf-8 -*-

import sqlite3
import sys

import datetime
import pandas as pd
from pandas import DataFrame
pd.set_option('display.max_rows', -1)




class db_open():
    def __init__(self):
        self.db_setting()
        self.input_value()
    def db_setting(self):
#        if sys.platform == 'linux':
            # path 추가
        self.db_name = "/root/loganal/agent/collect.db"
        #elif sys.platform == 'darwin':
        #    self.db_name = "/Users/jake/project/lp_log_analyzer/agent/collect.db"
    
    def df_setting(self,lp_df_temp):
        lp_api_call_df = DataFrame(lp_df_temp,
                                        columns=['일자','QR결제',
                                                 'QR결제취소',
                                                 '충전',
                                                 '하나카드결제',
                                                 '하나카드취소'
                                                 ])
	print("123:", lp_api_call_df)
	return lp_api_call_df
    def input_value(self):
        print("input start : ex) 202004030100")
        start_date = input()
        print("input end : ex) 202004040000")
        end_date = input()

        start_date = datetime.datetime.strptime(str(start_date), "%Y%m%d%H%M%S")
        end_date = datetime.datetime.strptime(str(end_date), "%Y%m%d%H%M%S")
        print(start_date)
        con = sqlite3.connect(self.db_name)
        cur = con.cursor()

        sql = "select date, localpay_order_payment_qrcode_count as QR결제, localpay_order_payment_cancel_count as QR취소, localpay_order_wallet_variation_charge_count as 충전, localpay_order_payment_card_count as 카드결제, localpay_order_payment_card_cancel_count as 카드취소 from lp_collector where date > ? and date <? order by date"
        cur.execute(sql, (start_date,end_date))
	 
        lp_df_temp=cur.fetchall()
        lp_df=self.df_setting(lp_df_temp)
        
        print(lp_df)
        # qr결제
        print("*******************************************************")
        print("조회 기간 : " +str(start_date) + " ~ " + str(end_date))

        sql = "select sum(localpay_order_payment_qrcode_count) from lp_collector where date > ? and date <?"
        cur.execute(sql, (start_date,end_date))
        self.localpay_order_payment_qrcode_count = cur.fetchall()[0][0]
        print("QR 결제: " + str(self.localpay_order_payment_qrcode_count))

        # qr 결제 취소
        sql = "select sum(localpay_order_payment_cancel_count) from lp_collector where date > ? and date <?"
        cur.execute(sql, (start_date,end_date))
        self.localpay_order_payment_cancel_count = cur.fetchall()[0][0]
        print("QR 결제 취소: " + str(self.localpay_order_payment_cancel_count))
        # select sum(localpay_order_payment_qrcode_count) from lp_collector where date > "2020-04-03 00:30:00" and date <"2020-04-04 00:50:00"

        # 충전
        sql = "select sum(localpay_order_wallet_variation_charge_count) from lp_collector where date > ? and date <?"
        cur.execute(sql, (start_date,end_date))
        self.localpay_order_wallet_variation_charge_count = cur.fetchall()[0][0]
        print("충전: " + str(self.localpay_order_wallet_variation_charge_count))

        # 카드 결제
        sql = "select sum(localpay_order_payment_card_count) from lp_collector where date > ? and date <?"
        cur.execute(sql, (start_date,end_date))
        self.localpay_order_payment_card_count = cur.fetchall()[0][0]
        print("카드 결제: " + str(self.localpay_order_payment_card_count))

        # 카드 결제 취소
        sql = "select sum(localpay_order_payment_card_cancel_count) from lp_collector where date > ? and date <?"
        cur.execute(sql, (start_date,end_date))
        self.localpay_order_payment_card_cancel_count = cur.fetchall()[0][0]
        print("카드 결제 취소: " + str(self.localpay_order_payment_card_cancel_count))
        print("*******************************************************")


if __name__ == "__main__":
    db_open()
